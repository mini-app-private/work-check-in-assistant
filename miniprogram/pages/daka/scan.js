// pages/daka/scan.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    type: null,
    scanData: null,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    console.log('onLoad options', options);
    const { type, title } = options || { type: 'SIGN_IN' };
    wx.setNavigationBarTitle({ title });
    this.setData({ type });
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 扫码结果回调
   * @param {*} res 
   */
  onScanCodeFunc(res) {
    const scanData = res.detail || { result: -1 };
    this.setData({ scanData });
    wx.navigateBack({
      success: () => {
        const eventChannel = this.getOpenerEventChannel();
        eventChannel.emit('ScanDataEvent', scanData.result);
      }
    });
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})