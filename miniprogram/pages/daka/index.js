// pages/daka/index.js
const { getDistance, showModal } = require('../../utils/index');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    daka_poi: {
      latitude: 0,
      longitude: 0,
      group_name: '未知打卡点',
      radius: 0,
    },
    user_poi: {
      latitude: 0,
      longitude: 0,
    },
    daka_list: [], // 当日打卡记录 SIGN_IN-签到 SIGN_OUT-签退
    next_type: 'SIGN_IN',
    daka_duration: 0, // 当日打卡累计时长
    daka_distance: 0, // 打卡距离
    today: '2024年1月1日',
    daka_max: 4, // 最多打卡次数
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    this.fetchTodayRecrodList();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
  },

  /**
   * 获取当天的打卡记录数据
   */
  fetchTodayRecrodList() {
    wx.showToast({ title: '加载中...', icon: 'loading' });
    wx.cloud.callFunction({ name: 'dakaFunctions', data: { type: 'todayList' } }).then(res => {
      if (res.errMsg !== 'cloud.callFunction:ok') throw res;
      return res.result;
    }).then(result => {
      console.log('fetchTodayRecrodList result', result);
      if (result.code !== 0) throw Error(result.msg);
      this.setData({
        daka_list: result.list || [],
        next_type: result.next_type || '"SIGN_IN"',
        daka_duration: result.duration || 0,
        today: result.today,
      });
      wx.hideToast();
    }).catch(err => {
      console.error('fetchTodayRecrodList Error', err);
      wx.hideToast();
      wx.showModal({
        title: '提示',
        content: err.message,
        showCancel: false,
      });
    });
  },

  /**
   * 跳转扫码打卡页面
   */
  goScanDakaFunc(event) {
    const titleMap = { SIGN_IN: '签到打卡', SIGN_OUT: '签退打卡' };
    const { next_type } = this.data;
    wx.navigateTo({
      url: `scan?type=${next_type}&title=${titleMap[next_type]}`,
      events: {
        ScanDataEvent: eData => {
          // wx.showModal({ content: eData });
          // 解析打卡点信息
          const daka_poi = this.parseScanDataFunc(eData);
          this.setData({ daka_poi });
          // 进入打卡流程
          this.preDakaFunc(daka_poi);
        },
      }
    });
  },

  /**
   * 解析扫码信息
   * 打卡点,36.69716878255208,117.2225425889757,20
   * group打卡点或组织名字,纬度,经度,打卡有效距离（单位：米）
   * @param {*} str 
   */
  parseScanDataFunc(str) {
    const result_array = str.split(',') || [];
    const group_name = result_array[0] || '未知打卡点'; // 打卡点名称
    const latitude = result_array[1] || 0; // 纬度
    const longitude = result_array[2] || 0; // 经度
    const radius = result_array[3] || 0; // 打卡有效半径
    return { group_name, latitude, longitude, radius: Number.parseInt(radius) };
  },

  /**
   * 打卡预处理
   * daka_poi.radius 为 0 时，表示不限制打卡距离
   * @param {*} daka_poi 打卡点信息
   */
  preDakaFunc(daka_poi) {
    // 1. 计算打卡距离，判断是否在设定的距离范围内
    if (daka_poi.radius === 0) { // 不限制打卡距离
      // 提示打卡
      wx.showModal({
        title: '提示',
        content: '现在要打卡吗？',
        confirmText: '打卡',
        cancelText: '取消',
        complete: (res) => {
          if (res.confirm) this.addDakaRecordFunc(daka_poi);
        }
      });
      return;
    }
    // 计算打卡距离，判断是否在打卡范围之内
    wx.getLocation().then(res => {
      const { latitude, longitude } = res || { latitude: 0,  longitude: 0}; // 用户当前经纬
      this.setData({ user_poi: { latitude, longitude } });
      const distance = getDistance(longitude, latitude, daka_poi.longitude, daka_poi.latitude);
      this.setData({ daka_distance: distance });
      if (distance > daka_poi.radius) { // 距离超出设定距离
        wx.showModal({
          title: '提示',
          content: '位置较远，请靠近后打卡。',
          showCancel: false
        });
      } else { // 在设定的距离内
        wx.showModal({
          title: '提示',
          content: '现在要打卡吗？',
          confirmText: '打卡',
          cancelText: '取消',
          complete: (res) => {
            if (res.confirm) this.addDakaRecordFunc(daka_poi);
          }
        });
      };
    }).catch(err => {
      wx.showModal({
        title: '提示',
        content: '用户位置信息获取失败，请重新打卡。',
        showCancel: false
      });
    });
  },

  /**
   * 提交打卡信息
   * { time: '2024年2月7日 08:30', type: 'SIGN_IN', distance: 0, daka_poi: {}, user_poi: {} }
   * @param {*} daka_poi 打卡点信息
   */
  addDakaRecordFunc(daka_poi) {
    wx.showToast({ title: '打卡中...', icon: 'loading' });
    const { next_type, daka_distance, user_poi } = this.data;
    const params = { type: next_type, distance: daka_distance, daka_poi, user_poi };
    const data = { type: 'add', params };
    wx.cloud.callFunction({name: 'dakaFunctions', data}).then(res => {
      wx.hideToast();
      wx.showModal({
        title: '提示',
        content: '打卡成功',
        showCancel: false,
        success: () => {
          // 刷新当天打卡数据
          this.fetchTodayRecrodList();
        }
      });
    }).catch(err => {
      wx.hideToast();
      wx.showModal({ title: '打卡失败', content: err.message, showCancel: false });
    });
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})