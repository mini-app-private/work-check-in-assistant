// pages/home/index.js
const ImageCdn = 'https://tdesign.gtimg.com/mobile/demos';
const swiperList = [
  `${ImageCdn}/swiper1.png`,
  `${ImageCdn}/swiper2.png`,
  `${ImageCdn}/swiper1.png`
];
Page({

  /**
   * 页面的初始数据
   */
  data: {
    current: 0,
    autoplay: true,
    duration: 500,
    interval: 5000,
    swiperList: [...swiperList],
    
    gridItemIcons01: 'https://tdesign.gtimg.com/mobile/demos/example1.png',
    gridItemIcons02: 'https://tdesign.gtimg.com/mobile/demos/example2.png',
    gridItemIcons03: 'https://tdesign.gtimg.com/mobile/demos/example3.png',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})