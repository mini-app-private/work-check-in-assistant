module.exports = {
  /**
   * 计算经纬度两点之间的距离，考虑球体
   * 单位 m(米)
   * @param {number} lng1 
   * @param {number} lat1 
   * @param {number} lng2 
   * @param {number} lat2 
   */
  getEarthDistance: function (lng1, lat1, lng2, lat2) {
    const EARTH_RADIUS = 6378.137;
    const radLng1 = rad(lng1);
    const radLat1 = rad(lat1);
    const radLng2 = rad(lng2);
    const radLat2 = rad(lat2);

    const latDiff = radLat1 - radLat2;
    const lngDiff = radLng1 - radLng2;

    let distance = 2 * Math.asin(Math.sqrt(Math.pow(Math.sin(latDiff / 2), 2) + Math.cos(radLat1) * Math.cos(radLat2) * Math.pow(Math.sin(lngDiff / 2), 2)));
    distance = distance * EARTH_RADIUS;
    distance = Math.round(distance * 10000);
    
    return distance
  },

  /**
   * 获取经纬度两点之间的直线距离，不考虑球体
   * 单位：米
   * @param {*} lng1 
   * @param {*} lat1 
   * @param {*} lng2 
   * @param {*} lat2 
   */
  getDistance: function (lng1, lat1, lng2, lat2) {
    const dx = lng1 - lng2;
    const dy = lat1 - lat2;
    const distance = Math.sqrt(dx*dx + dy*dy) * 1000;
    return distance;
  },

  /**
   * 模态提示弹框
   * @param {Object} params 
   */
  showModal: function (params) {
    const promise = new Promise((resolve, reject) => {
      wx.showModal({
        title: params.title || '提示',
        content: params.content || '提示内容',
        complete: (res) => {
          resolve(res);
        }
      })
    });
    return promise;
  }
};

/**
 * 经纬度转换
 * @param {number} du 
 */
function rad(du) {
  return du * Math.PI / 180.0;
}
