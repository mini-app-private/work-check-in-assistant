// 云函数入口文件
const cloud = require('wx-server-sdk');
const todayListFunc = require('./todayList/index');
const addFunc = require('./add/index');

cloud.init({ env: cloud.DYNAMIC_CURRENT_ENV }) // 使用当前云环境

// 云函数入口函数
exports.main = async (event, context) => {
  switch (event.type) {
    case 'add':
      return await addFunc.main(event, context);
    case 'todayList':
      return await todayListFunc.main(event, context);
    default:
      return { code: -1, msg: '未匹配到函数' };
  }
}